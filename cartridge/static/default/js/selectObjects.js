;
(function(module) {
    module.selectionSetup = function() {

        $(document).on('click', '.sysobj-checkbox', function() {
            var state = $(this).prop('checked'),
                systemObject = $(this).parent();

            $(systemObject).find('.group-checkbox').prop('checked', state);
            $(systemObject).find('.attribute-checkbox').prop('checked', state);
        })

        $(document).on('click', '.group-checkbox', function() {
            var state = $(this).prop('checked'),
                objectGroup = $(this).parent(),
                systemObject = $(objectGroup).parent();

            if (!state) {
                groupsChecked(systemObject);
            } else if (state) {
                $(systemObject).find('.sysobj-checkbox').prop('checked', state);
            }

            $(objectGroup).find('.attribute-checkbox').prop('checked', state);
        });

        $(document).on('click', '.attribute-checkbox', function() {
            var state = $(this).prop('checked'),
                objectGroup = $(this).parent().parent(),
                systemObject = $(objectGroup).parent();

            var groupCheckBox = $(objectGroup).find('.group-checkbox');

            if (!state) {
                var checkedAttributesCount = 0,
                    checkedGroupsCount = 0;

                $(objectGroup).find('.attribute-checkbox').each(function() {
                    if ($(this).prop('checked')) {
                        checkedAttributesCount++;
                    }
                });

                if (checkedAttributesCount === 0) {
                    $(groupCheckBox).prop('checked', state);
                }

                groupsChecked(systemObject);
            } else if (state) {
                $(objectGroup).find('.group-checkbox').prop('checked', state);
                $(objectGroup).parent().find('.sysobj-checkbox').prop('checked', state);
            }

        });

        $(document).on('click', '#export-button', function() {
            var url = $('#purl').data('url');

            var objects = [];
            var exportObjects = {};

            $('.attribute-checkbox').each(function() {
                if ($(this).prop('checked')) {
                    var objectName = $(this).data('object'),
                        groupName = $(this).data('group'),
                        attributeName = $(this).data('attribute'),
                        attributeType = $(this).data('type');

                    if (!exportObjects.hasOwnProperty(objectName)) {
                        exportObjects[objectName] = {
                            systemAttributes: {},
                            customAttributes: {},
                            groups: {}
                        }
                    }

                    if (!exportObjects[objectName].groups.hasOwnProperty(groupName) && groupName !== "Ungrouped") {
                        exportObjects[objectName].groups[groupName] = {
                            attributes: []
                        }
                    }

                    if (attributeType === "system") {
                        if (!exportObjects[objectName].systemAttributes.hasOwnProperty(attributeName)) {
                            exportObjects[objectName].systemAttributes[attributeName] = {
                                group: groupName
                            }

                            if (groupName !== "Ungrouped") {
                                exportObjects[objectName].groups[groupName].attributes.push(attributeName);
                            }
                        }
                    }

                    if (attributeType === "custom") {
                        if (!exportObjects[objectName].customAttributes.hasOwnProperty(attributeName)) {
                            exportObjects[objectName].customAttributes[attributeName] = {
                                group: groupName
                            }

                            if (groupName !== "Ungrouped") {
                                exportObjects[objectName].groups[groupName].attributes.push(attributeName);
                            }
                        }
                    }

                }
            });

            $.ajax({
                method: "POST",
                url: url,
                data: {
                    objects: JSON.stringify(exportObjects)
                },
                dataType: 'json',

                success: function() {
                    alert("Exported")
                }
            });

        });

        function groupsChecked(systemObject) {
            var checkedGroupsCount = 0;

            $(systemObject).find('.group-checkbox').each(function() {
                if ($(this).prop('checked')) {
                    checkedGroupsCount++;
                }
            });

            if (checkedGroupsCount === 0) {
                $(systemObject).find('.sysobj-checkbox').prop('checked', state);
            }
        }

    }

}(window.app = window.app || {}));
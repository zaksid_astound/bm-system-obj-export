'use strict';

/**
 * Controller that manages System Object Definitions.
 *
 * @module controllers/SystemObjectsExport
 */

/* API Includes */
var ISML = require('dw/template/ISML'),
    Pipelet = require('dw/system/Pipelet'),
    File = require('dw/io/File'),
    FileReader = require('dw/io/FileReader'),
    FileWriter = require('dw/io/FileWriter'),
    XMLStreamReader = require('dw/io/XMLStreamReader'),
    XMLStreamWriter = require('dw/io/XMLStreamWriter'),
    XMLStreamConstants = require('dw/io/XMLStreamConstants'),
    ObjectTypeDefinition = require('dw/object/ObjectTypeDefinition');

/* Script Modules */
var app = require('~/cartridge/scripts/app'),
    guard = require('~/cartridge/scripts/guard');

/**
 * Renders a page with the list of System Object Definitions.
 *
 * Creates a PagingModel for the orders with information from the httpParameterMap.
 * Invalidates and clears the orders.orderlist form. Updates the page metadata. Sets the
 * ContinueURL property to Order-Orders and renders the order history page (account/orderhistory/orders template).
 */
function show() {
    var filename = "expsysobj.xml";
    var status = exportSystemObjectDefinitions(filename);

    if (status === 0) {
        var systemObjects = readXML(filename);

        ISML.renderTemplate('systemobjectslist', {
            SystemObjectDefinitions: systemObjects
        });
    }
}

/**
 * Exports System Object Definitions to IMPEX.
 *
 * Creates an XML file in IMPEX containing System Object Definitions.
 */


/**
 * Exports System Object Definitions to an XML file.
 *
 * Creates an XML file containing System Object Definitions.
 * Returns status of operation.
 *
 * @returns {Number}
 */
function exportSystemObjectDefinitions(filename) {
    var errorCode, errorMessage;
    var ExportMetaData = new Pipelet('ExportMetaData').execute({
        ExportFile: filename,
        ExportSystemTypes: true,
        ExportCustomTypes: false,
        ExportCustomPreferences: false,

        ErrorCode: errorCode,
        ErrorMsg: errorMessage
    });

    return ExportMetaData.ErrorCode;
}

/**
 * Reads needed fields from XML file System Object Definitions.
 *
 * Reads needed fields from XML file containing System Object Definitions to an object.
 * XML file is situated in IMPEX.
 * Returns object with System Object Definitions.
 *
 * @returns {Object} Object containing System Object Definitions.
 */
function readXML(filename) {
    var file = new File(File.IMPEX + "/src/" + filename),
        fileReader = new FileReader(file, "UTF-8"),
        xmlStreamReader = new XMLStreamReader(fileReader);

    var systemObjectDefinitions = [],
        systemObject = {},
        nextTag;

    while (xmlStreamReader.hasNext()) {
        if (nextTag === XMLStreamConstants.CHARACTERS) {
            nextTag = xmlStreamReader.next();
        } else {
            try {
                nextTag = xmlStreamReader.nextTag();
            } catch (e) {
                xmlStreamReader.close();
                fileReader.close();
                return systemObjectDefinitions;
            }
        }

        var tagName = (nextTag === XMLStreamConstants.START_ELEMENT) ? xmlStreamReader.getLocalName() : '';
        var attributeType;

        if (nextTag === XMLStreamConstants.START_ELEMENT) {
            switch (tagName) {
                case "system-attribute-definitions":
                    attributeType = "system";
                    break;

                case "custom-attribute-definitions":
                    attributeType = "custom";
                    break;

                case "type-extension":
                    if (systemObject.attributes && systemObject.groups) {
                        var group = {
                            name: "Ungrouped",
                            attributes: systemObject.attributes
                        };
                        systemObject.groups.push(group);
                    }
                    systemObjectDefinitions.push(systemObject);

                    var name = xmlStreamReader.getAttributeValue(0);
                    systemObject = {
                        name: name,
                        attributes: [],
                        groups: []
                    };

                    break;

                case "attribute-definition":
                    var name = xmlStreamReader.getAttributeValue(0);

                    var attribute = {
                        name: name,
                        type: attributeType
                    };

                    systemObject.attributes.push(attribute);
                    break;

                case "attribute-group":
                    var name = xmlStreamReader.getAttributeValue(0);
                    var group = {
                        name: name,
                        attributes: []
                    };
                    systemObject.groups.push(group);
                    break;

                case "attribute":
                    var name = xmlStreamReader.getAttributeValue(0);
                    for (var i = 0; i < systemObject.attributes.length; i++) {
                        if (systemObject.attributes[i].name === name) {
                            group.attributes.push(systemObject.attributes[i]);
                            systemObject.attributes.splice(i, 1);
                        }
                    }
                    break;

                default:
                    nextTag = xmlStreamReader.next();
                    break;
            }
        }

    }

    xmlStreamReader.close();
    fileReader.close();

    return systemObjectDefinitions;
}

function start() {
    var filename = "expsysobj.xml";
    var inputFile = new File(File.IMPEX + "/src/" + filename),
        fileReader = new FileReader(inputFile, "UTF-8"),
        xmlStreamReader = new XMLStreamReader(fileReader),
        filename = "exportedOrders.xml", //_" + dw.util.UUIDUtils.createUUID() + ".xml",

        outputFile = new File(File.IMPEX + "/src/" + filename),
        fileWriter = new FileWriter(outputFile, "UTF-8"),
        xsw = new XMLStreamWriter(fileWriter);

    var objects = JSON.parse(request.httpParameterMap.objects.stringValue);

    xsw.writeStartDocument("UTF-8", "1.0");

    xsw.writeStartElement("metadata");
    xsw.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/metadata/2006-10-31");

    var nextTag, currentObject, currentAttribute;

    while (xmlStreamReader.hasNext()) {
        if (nextTag === XMLStreamConstants.CHARACTERS) {
            nextTag = xmlStreamReader.next();
        } else {
            nextTag = xmlStreamReader.nextTag();
        }

        var tagName = (nextTag === XMLStreamConstants.START_ELEMENT) ? xmlStreamReader.getLocalName() : '';

        if (nextTag === XMLStreamConstants.START_ELEMENT) {
            switch (tagName) {
                case "type-extension":
                    var name = xmlStreamReader.getAttributeValue(0);

                    if (objects.hasOwnProperty(name)) {
                        xsw.writeStartElement("type-extension");
                        xsw.writeAttribute("type-id", name);
                        currentObject = objects[name];
                    }

                    break;

                case "custom-attribute-definitions":
                    if (currentObject && Object.keys(currentObject.customAttributes).length > 0) {
                        xsw.writeStartElement("custom-attribute-definitions");
                        xsw.writeCharacters("\n");
                    }
                    break;

                case "attribute-definition":
                    var name = xmlStreamReader.getAttributeValue(0);

                    if (currentObject && currentObject.customAttributes.hasOwnProperty(name)) {
                        var myObject = xmlStreamReader.getXMLObject().toString();
                        xsw.writeRaw(myObject);
                        currentAttribute = currentObject.customAttributes[name];
                    }

                    break;

                default:
                    nextTag = xmlStreamReader.next();
                    break;
            }
        }

        if (nextTag === XMLStreamConstants.END_ELEMENT) {
            var name = xmlStreamReader.getLocalName();
            switch (name) {
                case "custom-attribute-definitions":
                    if (currentAttribute) {
                        xsw.writeEndElement();
                        currentAttribute = undefined;
                    }
                    break;

                case "type-extension":
                    if (currentObject) {
                        if (Object.keys(currentObject.groups).length > 0) {
                            xsw.writeStartElement("group-definitions");

                            for (var group in currentObject.groups) {
                                if (group !== "Ungrouped") {
                                    xsw.writeStartElement("attribute-group");
                                    xsw.writeAttribute("group-id", group);

                                    var attr = JSON.parse(currentObject.groups[group].attributes);
                                    for (var i = 0; i < attr.length; i++) {
                                        xsw.writeStartElement("attribute");
                                        xsw.writeAttribute("attribute-id", attr[i]);
                                        xsw.writeEndElement(); // attribute
                                    }

                                    xsw.writeEndElement(); // attribute-group
                                }
                            }

                            xsw.writeEndElement(); // group-definitions
                        }

                        xsw.writeEndElement();

                        currentObject = undefined;
                    }
                    break;

                default:
                    nextTag = xmlStreamReader.next();
                    break;
            }
        }

    }

    xsw.writeEndElement(); // metadata
    xsw.writeEndDocument();

    xsw.close();
    xmlStreamReader.close();
    fileReader.close();
    fileWriter.close();
}


/**
 * Render template systemobjectslist
 * @see {@link module:controllers/SystemObjectsExport~show}
 */
exports.Show = guard.ensure(['get'], show);
exports.Start = guard.ensure(['post'], start);